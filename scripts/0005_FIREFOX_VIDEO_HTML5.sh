# Trying the version 29 of Fedora linux distribution I had issues to run HTML5 content in Firefox.
#
# Some librairies are not installed by default because of license issue (https://fedoraproject.org/wiki/Forbidden_items).
#
# Install RPMFusion repositories
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# and then to install the required librairies:
sudo dnf install -y ffmpeg-libs

# After restarting Firefox you should be able to watch HTML5 content.
