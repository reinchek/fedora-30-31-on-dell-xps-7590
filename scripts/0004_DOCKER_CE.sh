# https://fedoraproject.org/wiki/Common_F31_bugs#Docker_package_no_longer_available_and_will_not_run_by_default_.28due_to_switch_to_cgroups_v2.29
#
# Docker package no longer available and will not run by default (due to switch to cgroups v2)
#
# Fedora 31 uses Cgroups v2 by default. 
# The moby-engine package does not support Cgroups v2 yet, so if you need to run the moby-engine or run the Docker CE package, 
# then you need to switch the system to using Cgroups v1, by passing the kernel parameter systemd.unified_cgroup_hierarchy=0. 
# To do this permanently, run sudo grub2-editenv - set "$(grub2-editenv - list | grep kernelopts) systemd.unified_cgroup_hierarchy=0".

sudo grub2-editenv - set "$(grub2-editenv - list | grep kernelopts) systemd.unified_cgroup_hierarchy=0"

