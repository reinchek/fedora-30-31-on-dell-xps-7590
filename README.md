# Fedora 30-31 on Dell XPS 7590

This repo wants to be a place where all solutions, about Fedora Linux on dell xps 7590 issues, are collected.

* Folder structure
    * **scripts**: all bash scripts with commands to execute to resolves issues.
    * **packages**: packages to install, modify or usefull to resolve a specific issue. Are contempled rpm or package to compile.
        * All packages might be ran also with **toolbox** ( https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox )
* For each issue creates a new branch starting from *dev* named *issue/ISSUE-ID* (based on below issues list)

### Issues list
| Status | ISSUE-ID | Title | Description | Fedora version |
| ------ | ------ | ------ | ----------- | ------------ |
| ✘ | 0001_NVIDIA | NVIDIA Driver | NVIDIA Driver installation for GeForce GTX 1650 card. | 30/31 |
| ✘ | 0002_FINGERPRINT | FingerPrint | Try to make work fingerprint sensor **27c6:5395** HTMicroelectronics Goodix Fingerprint Device | 30/31 |
| ✘ | 0003_TEMPERATURE | High temp   | Sometimes happens the overthermal CPU advice. Try to resolve it. | 30/31 |
| ✔ | [0004_DOCKER_CE](https://gitlab.com/reinchek/fedora-30-31-on-dell-xps-7590/commit/4ab75328521f2a682f236192252dab8583bc71c3)   | Docker CE on F31| Docker package no longer available and will not run by default (due to switch to cgroups v2) | 31 |
| ✔ | [0005_FIREFOX_VIDEO_HTML5](https://gitlab.com/reinchek/fedora-30-31-on-dell-xps-7590/commit/e6193f59b60bc0b0a0b84e94657877e0d620ed7a) | Video Youtube HTML5 | When try to open a YouTube video using Firefox 70.0 (64 bit) it's' shown an error about HTML5 player. | 29/30/31 |

### Usefull links
1.  https://fedoraproject.org/wiki/Common_F31_bugs

**Legenda:**
*  ✘  not resolved
*  ✔  already resolved
